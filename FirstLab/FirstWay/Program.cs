﻿using System;
using System.Runtime.InteropServices.ComTypes;

namespace FirstLab.FirstWay
{
    class Program
    {
        /*static*/ void Main(string[] args)
        {
            ModelWindow window = Init();
            
            Read(window);
            
            Display(window);
            
            Console.WriteLine("Moving window right");
            MoveHorizontal(window, 500);
            
            Display(window);
            
            Console.WriteLine("Нажмите Enter для выхода...");
            Console.ReadLine();
        }
        
        public static ModelWindow Init()
        {
            return new ModelWindow();
        }

        public static void Read(ModelWindow window)
        {
            Console.Write("Input header: ");
            window.Header = Console.ReadLine();

            Console.Write("Input x coordinate of left top corner: ");
            window.LeftTopCornerX = int.TryParse(Console.ReadLine(), out var temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input y coordinate of left top corner: ");
            window.LeftTopCornerY = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input color: ");
            window.Color = Console.ReadLine();
            
            Console.Write("Input horizontal window size: ");
            window.HorizontalSize = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input vertical window size: ");
            window.VerticalSize = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input flag of window visibility: ");
            window.IsVisible = bool.TryParse(Console.ReadLine(), out var temporalFlag) && temporalFlag;
            
            Console.Write("Input flag of frame visibility: ");
            window.FrameState = bool.TryParse(Console.ReadLine(), out temporalFlag) && temporalFlag;
        }

        public static string ToString(ModelWindow window)
        {
            string result = $"Window {window.Header}";

            return result;
        }

        public static void Display(ModelWindow window)
        {
            Console.WriteLine($"Window {window.Header}\nCoordinates ({window.LeftTopCornerX}, {window.LeftTopCornerY})");
            Console.WriteLine($"Vertical size {window.VerticalSize},\tHorizontal size {window.HorizontalSize}");
            string visible = window.IsVisible ? "" : "in";
            string showFrame = window.FrameState ? "with" : "without";
            Console.WriteLine($"Color {window.Color}\t {visible}visible, {showFrame} frame");
        }
        
        public static void MoveHorizontal(ModelWindow window, int dx) 
        {
            if (dx > 0)
            {
                int newX = (window.HorizontalSize + window.LeftTopCornerX + dx) > Constants.HORIZONTAL_SCREEN_SIZE
                    ? Constants.HORIZONTAL_SCREEN_SIZE - window.HorizontalSize
                    : window.LeftTopCornerX + dx;
                window.LeftTopCornerX = newX;
            }
            else
            {
                int newX = (window.LeftTopCornerX + dx) < 0
                    ? 0 : window.LeftTopCornerX + dx;
                window.LeftTopCornerX = newX;
            }
        }
        
        public static void MoveVertical(ModelWindow window, int dy) 
        {
            if (dy > 0)
            {
                int newY = (window.VerticalSize + window.LeftTopCornerY + dy) > Constants.VERTICAL_SCREEN_SIZE
                    ? Constants.VERTICAL_SCREEN_SIZE - window.VerticalSize
                    : window.LeftTopCornerY + dy;
                window.LeftTopCornerY = newY;
            }
            else
            {
                int newY = (window.LeftTopCornerY + dy) < 0
                    ? 0 : window.LeftTopCornerY + dy;
                window.LeftTopCornerY = newY;
            }
        }

        public static void Resize(ModelWindow window, int newX = 0, int newY = 0)
        {
            if (newX > 0)
            {
                int newSize = window.LeftTopCornerX + newX > Constants.HORIZONTAL_SCREEN_SIZE
                    ? Constants.HORIZONTAL_SCREEN_SIZE - window.LeftTopCornerX
                    : newX;
                window.HorizontalSize = newSize;
            }

            if (newY > 0)
            {
                int newSize = window.LeftTopCornerY + newY > Constants.VERTICAL_SCREEN_SIZE
                    ? Constants.VERTICAL_SCREEN_SIZE - window.LeftTopCornerY
                    : newY;
                window.VerticalSize = newSize;
            }
        }

        public static void ChangeColor(ModelWindow window, string newColor)
        {
            window.Color = newColor;
        }

        public static void ChangeState(ModelWindow window)
        {
            window.IsVisible = !window.IsVisible;
        }
        
        public static void ChangeFrameState(ModelWindow window)
        {
            Console.Write("Input flag of frame visibility: ");
            window.FrameState = bool.TryParse(Console.ReadLine(), out var temporalFlag) && temporalFlag;
        }
    }
}