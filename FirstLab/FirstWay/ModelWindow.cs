namespace FirstLab.FirstWay
{
    public class ModelWindow
    {
        public string Header { get; set; }
        public int LeftTopCornerX { get; set; }
        public int LeftTopCornerY { get; set; }
        public int VerticalSize { get; set; }
        public int HorizontalSize { get; set; }
        public string Color { get; set; }
        public bool IsVisible { get; set; }
        public bool FrameState { get; set; }
        
        
                                        
    }
}