using System;

namespace FirstLab.SecondWay
{
    public class Program
    {
        /*static*/ void Main(string[] args)
        {
            ModelWindow window = ModelWindow.Init();
            
            window.Read();
            
            window.Display();
            
            Console.WriteLine("Moving the window right: 500");
            window.MoveHorizontal(500);
            Console.WriteLine("Resizing the window vertically up to 123");
            window.Resize(newY: 123);
            Console.WriteLine("Resizing the window horizontally up to 123");
            window.Resize(newX: 256);

            Console.WriteLine("Changing state of the window");
            window.ChangeState();

            Console.WriteLine("Changing color of the window");
            window.ChangeColor("#FF00FF");
            
            window.Display();
            
            Console.WriteLine("Нажмите Enter для выхода...");
            Console.ReadLine();
        }
    }
}