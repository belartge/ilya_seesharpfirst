using System;

namespace FirstLab.SecondWay
{
    public class ModelWindow
    {
        public string Header { get; set; }
        public int LeftTopCornerX { get; set; }
        public int LeftTopCornerY { get; set; }
        public int VerticalSize { get; set; }
        public int HorizontalSize { get; set; }
        public string Color { get; set; }
        public bool IsVisible { get; set; }
        public bool FrameState { get; set; }
        
        public static ModelWindow Init()
        {
            return new ModelWindow();
        }

        public void Read()
        {
            Console.Write("Input header: ");
            Header = Console.ReadLine();

            Console.Write("Input x coordinate of left top corner: ");
            LeftTopCornerX = int.TryParse(Console.ReadLine(), out var temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input y coordinate of left top corner: ");
            LeftTopCornerY = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input color: ");
            Color = Console.ReadLine();
            
            Console.Write("Input horizontal window size: ");
            HorizontalSize = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input vertical window size: ");
            VerticalSize = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input flag of window visibility: ");
            IsVisible = bool.TryParse(Console.ReadLine(), out var temporalFlag) && temporalFlag;
            
            Console.Write("Input flag of frame visibility: ");
            FrameState = bool.TryParse(Console.ReadLine(), out temporalFlag) && temporalFlag;
        }

        public override string ToString()
        {
            string result = $"Window {Header}";

            return result;
        }

        public void Display()
        {
            Console.WriteLine($"Window {Header}\nCoordinates ({LeftTopCornerX}, {LeftTopCornerY})");
            Console.WriteLine($"Vertical size {VerticalSize},\tHorizontal size {HorizontalSize}");
            string visible = IsVisible ? "" : "in";
            string showFrame = FrameState ? "with" : "without";
            Console.WriteLine($"Color {Color}\t {visible}visible, {showFrame} frame");
        }
        
        public void MoveHorizontal(int dx) 
        {
            if (dx > 0)
            {
                int newX = (HorizontalSize + LeftTopCornerX + dx) > Constants.HORIZONTAL_SCREEN_SIZE
                    ? Constants.HORIZONTAL_SCREEN_SIZE - HorizontalSize
                    : LeftTopCornerX + dx;
                LeftTopCornerX = newX;
            }
            else
            {
                int newX = (LeftTopCornerX + dx) < 0
                    ? 0 : LeftTopCornerX + dx;
                LeftTopCornerX = newX;
            }
        }
        
        public void MoveVertical(int dy) 
        {
            if (dy > 0)
            {
                int newY = (VerticalSize + LeftTopCornerY + dy) > Constants.VERTICAL_SCREEN_SIZE
                    ? Constants.VERTICAL_SCREEN_SIZE - VerticalSize
                    : LeftTopCornerY + dy;
                LeftTopCornerY = newY;
            }
            else
            {
                int newY = (LeftTopCornerY + dy) < 0
                    ? 0 : LeftTopCornerY + dy;
                LeftTopCornerY = newY;
            }
        }

        public void Resize(int newX = 0, int newY = 0)
        {
            if (newX > 0)
            {
                int newSize = LeftTopCornerX + newX > Constants.HORIZONTAL_SCREEN_SIZE
                    ? Constants.HORIZONTAL_SCREEN_SIZE - LeftTopCornerX
                    : newX;
                HorizontalSize = newSize;
            }

            if (newY > 0)
            {
                var newSize = LeftTopCornerY + newY > Constants.VERTICAL_SCREEN_SIZE
                    ? Constants.VERTICAL_SCREEN_SIZE - LeftTopCornerY
                    : newY;
                VerticalSize = newSize;
            }
        }

        public void ChangeColor(string newColor)
        {
            Color = newColor;
        }

        public void ChangeState()
        {
            IsVisible = !IsVisible;
        }
        
        public void ChangeFrameState()
        {
            Console.Write("Input flag of frame visibility: ");
            FrameState = bool.TryParse(Console.ReadLine(), out var temporalFlag) && temporalFlag;
        }
    }
}