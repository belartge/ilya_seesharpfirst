using System;

namespace FirstLab.ThirdWay
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ModelWindowShell windowShell = new ModelWindowShell();
        
            windowShell.Init();
            
            windowShell.Read();
            
            windowShell.Display();
            
            Console.WriteLine();
            Console.WriteLine("Moving the window right: 500");
            windowShell.window.MoveHorizontal(500);
            windowShell.Display();
            
            Console.WriteLine();
            Console.WriteLine("Resizing the window vertically up to 123");
            windowShell.window.Resize(newY: 123);
            windowShell.Display();
                        
            Console.WriteLine();
            Console.WriteLine("Resizing the window horizontally up to 123");
            windowShell.window.Resize(newX: 256);
            windowShell.Display();

            Console.WriteLine();
            Console.WriteLine("Changing state of the window");
            windowShell.window.ChangeState();
            windowShell.Display();

            Console.WriteLine();
            Console.WriteLine("Changing color of the window");
            windowShell.window.ChangeColor("#FF00FF");
            windowShell.Display();
            
            Console.WriteLine("Нажмите Enter для выхода...");
            Console.ReadLine();
        }
    }
}