using System;
using System.Drawing;

namespace FirstLab.ThirdWay
{
    public class ModelWindowShell
    {
        public ModelWindow window;
        
        public void Init()
        {
            window = new ModelWindow();
        }

        public void Read()
        {
            Console.Write("Input header: ");
            window.Header = Console.ReadLine();

            Console.Write("Input x coordinate of left top corner: ");
            window.LeftTopCornerX = int.TryParse(Console.ReadLine(), out var temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input y coordinate of left top corner: ");
            window.LeftTopCornerY = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input color: ");
            window.Color = Console.ReadLine();
            
            Console.Write("Input horizontal window size: ");
            window.HorizontalSize = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input vertical window size: ");
            window.VerticalSize = int.TryParse(Console.ReadLine(), out temporalNumber) ? temporalNumber : 0;
            
            Console.Write("Input flag of window visibility: ");
            window.IsVisible = bool.TryParse(Console.ReadLine(), out var temporalFlag) && temporalFlag;
            
            Console.Write("Input flag of frame visibility: ");
            window.FrameState = bool.TryParse(Console.ReadLine(), out temporalFlag) && temporalFlag;
        }

        public override string ToString()
        {
            string result = $"Window {window.Header}";

            return result;
        }

        public void Display()
        {
            Console.WriteLine($"Window {window.Header}\nCoordinates ({window.LeftTopCornerX}, {window.LeftTopCornerY})");
            Console.WriteLine($"Vertical size {window.VerticalSize},\tHorizontal size {window.HorizontalSize}");
            string visible = window.IsVisible ? "" : "in";
            string showFrame = window.FrameState ? "with" : "without";
            Console.WriteLine($"Color {window.Color}\t {visible}visible, {showFrame} frame");
        }
    }
}