using System;

namespace FirstLab.ThirdWay
{
    public class ModelWindow
    {
        public string Header { get; set; }
        public int LeftTopCornerX { get; set; }
        public int LeftTopCornerY { get; set; }
        public int VerticalSize { get; set; }
        public int HorizontalSize { get; set; }
        public string Color { get; set; }
        public bool IsVisible { get; set; }
        public bool FrameState { get; set; }
        
        public void MoveHorizontal(int dx) 
        {
            if (dx > 0)
            {
                int newX = (HorizontalSize + LeftTopCornerX + dx) > Constants.HORIZONTAL_SCREEN_SIZE
                    ? Constants.HORIZONTAL_SCREEN_SIZE - HorizontalSize
                    : LeftTopCornerX + dx;
                LeftTopCornerX = newX;
            }
            else
            {
                int newX = (LeftTopCornerX + dx) < 0
                    ? 0 : LeftTopCornerX + dx;
                LeftTopCornerX = newX;
            }
        }
        
        public void MoveVertical(int dy) 
        {
            if (dy > 0)
            {
                int newY = (VerticalSize + LeftTopCornerY + dy) > Constants.VERTICAL_SCREEN_SIZE
                    ? Constants.VERTICAL_SCREEN_SIZE - VerticalSize
                    : LeftTopCornerY + dy;
                LeftTopCornerY = newY;
            }
            else
            {
                int newY = (LeftTopCornerY + dy) < 0
                    ? 0 : LeftTopCornerY + dy;
                LeftTopCornerY = newY;
            }
        }

        public void Resize(int newX = 0, int newY = 0)
        {
            if (newX > 0)
            {
                int newSize = LeftTopCornerX + newX > Constants.HORIZONTAL_SCREEN_SIZE
                    ? Constants.HORIZONTAL_SCREEN_SIZE - LeftTopCornerX
                    : newX;
                HorizontalSize = newSize;
            }

            if (newY > 0)
            {
                var newSize = LeftTopCornerY + newY > Constants.VERTICAL_SCREEN_SIZE
                    ? Constants.VERTICAL_SCREEN_SIZE - LeftTopCornerY
                    : newY;
                VerticalSize = newSize;
            }
        }

        public void ChangeColor(string newColor)
        {
            Color = newColor;
        }

        public void ChangeState()
        {
            IsVisible = !IsVisible;
        }
        
        public void ChangeFrameState()
        {
            Console.Write("Input flag of frame visibility: ");
            FrameState = bool.TryParse(Console.ReadLine(), out var temporalFlag) && temporalFlag;
        }
    }
}